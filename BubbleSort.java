public class BubbleSort {

    public static void main(String[] args) {
        int[] numeros = {5, 3, 4, 2, 1, 8, 7, 6};
        ordenarBurbuja(numeros);
        System.out.println("Array ordenado:");
        for (int numero : numeros) {
            System.out.print(numero + " ");
        }
    }

    public static void ordenarBurbuja(int[] numeros) {
        boolean cambiado = true;
        for (int i = 0; i < numeros.length - 1 && cambiado; i++) {
            cambiado = false;
            for (int j = 0; j < numeros.length - 1 - i; j++) {
                if (numeros[j] > numeros[j + 1]) {
                    int temp = numeros[j];
                    numeros[j] = numeros[j + 1];
                    numeros[j + 1] = temp;
                    cambiado = true;
                }
            }
        }
    }
}

/*Tenemos un array sin ordenar que contiene n elementos.
Comenzamos por el par de los dos primeros elementos, comparamos sus valores y los intercambiamos 
si no están en el orden correcto. Repetimos el proceso para cada par de elementos, avanzando de
izquierda a derecha en la array. El elemento más pequeño/más grande está en la última posición 
después de este paso.Repetimos los pasos anteriores (n-2) veces para las iteraciones restantes. 

Reducimos el tamaño de la array en uno cada vez, ya que el último elemento ya está ordenado. 
El elemento más pequeño/más grande en esta iteración se mueve a la posición más a la derecha.
En el algoritmo para ordenar un array de tamaño n, se necesitan n-1 pases. El array
(5, 3, 4, 2, 1, 8, 7, 6) se ordena utilizando el algoritmo de ordenamiento de burbuja. 
 Después de la cuarta pasada, obtenemos el array ordenado: (1, 2, 3, 4, 5, 6, 7, 8).*/