import java.util.Arrays;

public class BubbleSortWithEvenNumbers {
    public static void bubbleSort(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    // Intercambiar elementos si están en el orden incorrecto
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static int[] selectEvenNumbers(int[] arr) {
        int count = 0;
        for (int num : arr) {
            if (num % 2 == 0) {
                count++;
            }
        }
        int[] evenNumbers = new int[count];
        int index = 0;
        for (int num : arr) {
            if (num % 2 == 0) {
                evenNumbers[index] = num;
                index++;
            }
        }
        return evenNumbers;
    }

    public static void main(String[] args) {
        int[] arr = {5, 3, 4, 2, 1, 8, 7, 6};
        System.out.println("Array original: " + Arrays.toString(arr));
        bubbleSort(arr);
        System.out.println("Array ordenado: " + Arrays.toString(arr));

        int[] evenNumbers = selectEvenNumbers(arr);
        System.out.println("Números pares seleccionados: " + Arrays.toString(evenNumbers));
    }
}

/*Hemos agregado una nueva función llamada selectEvenNumbers.

Esta función recorre el array original y cuenta cuántos números pares hay. Luego, crea un nuevo
array llamado evenNumbers del tamaño adecuado para almacenar los números pares. Otra iteración a
través del array original copia los números pares al nuevo array. Finalmente, el nuevo array contiene 
solo los números pares del array original.

En el algoritmo proporcionado, el array original (5, 3, 4, 2, 1, 8, 7, 6) se ordena utilizando
el algoritmo de ordenamiento de burbuja. Después de la cuarta pasada, obtenemos el array ordenado: 
(1, 2, 3, 4, 5, 6, 7, 8). Luego, seleccionamos los números pares y obtenemos el array: (2, 4, 6, 8). */
